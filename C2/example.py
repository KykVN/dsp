import numpy as np
import func

u,n = func.sigseq(0,-10,10)
d = [0 if i!=0 else 1 for i in n]

x = func.shiftValue(u,1)-func.shiftValue(u,-3)+d;
h = 2*(u-func.shiftValue(u,-3))

# print(x)
# print(h)

y = func.convolve(x,h)
print(np.random.randn(51))

# func.show(y,n,'Ex1')
