import numpy as np
import matplotlib.pyplot as pp
from scipy import signal



def shiftValue(x,a,val=None):
    if (a>0):
        x = np.append(x[a:],(np.full(a,x[len(x)-1] if val is None else val)))
    elif (a<0):
        x = np.insert(x, 0, (np.full(abs(a),x[0] if val is None else val)))[:a]
    return x
def flipValue(x,n):
    x1 = np.flip(x)
    gap = n[0]+n[len(n)-1]
    x1 = shiftValue(x1,gap)
    return x1;
def show(xa, na, name,xlabel='n',ylabel='x'):
    pp.plot(na, xa)
    pp.title(name)
    pp.xlabel(xlabel)
    pp.ylabel(ylabel)
    pp.show()
def multiply(x,n,val):
    x1 = np.zeros(len(n),dtype=int)
    for j in range(len(n)):
        if(n[j]%val==0):
            pos = int(-(n[j]-n[j]/val)+j)
            x1[pos]=x[j]
    return x1

def sigseq(n0,l,r):
    n = np.arange(l,r+1)
    u = [1 if i-n0>=0 else 0 for i in n]
    return u,n
def convolve(x,h):
    return signal.convolve(x,h,mode='same')
def exp(a,n):
    return np.exp(n*np.log(a))