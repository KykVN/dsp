import numpy as np
import func

# n = np.arange(-10, 10)
# u = [1 if i>=0 else 0 for i in n]

u,n = func.sigseq(0,-10,10)

y1 = 2*func.shiftValue(u,-2)
y2 = 2*func.shiftValue(u,-7)
y3 = 2*func.flipValue(u,n)
y4 = 2*func.flipValue(func.shiftValue(u,4),n)
y = y1-y2-y3+y4
func.show(y,n,'Y',ylabel='y')

# ========= A =========
ya=2-3*y
func.show(ya,n,'HW2 A',ylabel='y')

# ========= B =========
yb = func.shiftValue(y,-2)
yb=yb*3;
func.show(yb,n,'HW2 B',ylabel='y')

# ========= B =========
yc = func.shiftValue(y,-2)
yc = 2-2*yc
func.show(yc,n,'HW2 C2',ylabel='y')