import numpy as np
import func

u,n = func.sigseq(0,-10,10)

u1 = func.shiftValue(u,1)
u2 = func.shiftValue(u,-5)
print(func.shiftValue(u,2))
u3 = func.flipValue(func.shiftValue(u,2),n)
x = (u1-u2)*(n * u3)


func.show(x,n,'Exercise')
