import numpy as np
import func

n = np.arange(-10, 10)
x = np.zeros(len(n),dtype=int)
for i in range(len(n)):
    if n[i]==-1:
        x[i]=-2
        x[i+2]=-2
        x[i+1]=4
        x[i+3]=2
        x[i+4]=-1
        break

# ========= A =========
xa = func.shiftValue(x,-1)
xa = func.flipValue(xa,n)
xa=3*xa
func.show(xa,n,'HW1 A')

# ========= B =========
xb = func.multiply(x,n,2)
xb=xb-1;
func.show(xb,n,'HW1 B')

# ========= B =========
xc = -x
xc = xc+2
func.show(xc,n,'HW C')
